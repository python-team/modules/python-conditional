Changelog
=========

1.1 - 2014-04-19
----------------

- Remove setuptools from install_requires because it isn't.
  [stefan]

1.0 - 2012-05-16
----------------

- Initial release.
  [stefan]
